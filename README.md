pcomb
=====

`pcomb` is a **Parser Combinator** library for python that includes type annotations. Programs
written using pcomb can be checked using a python type-checker like
[mypy](https://mypy.readthedocs.io) or [pyright](https://github.com/microsoft/pyright).

About
-----

There are several excellent parsing libraries for python, but none of them provide type
annotations and users need to supply their own or lose the benefits of static analysis on their
parsing code. Parser combinators are incredibly expressive constructs which can be used to easily
build recursive-descent parsers, and can be quickly created using only a handful of primitives.
Parser combinators allow users to express their parsers as code rather than as grammar files.
The goal of the `pcomb` library is to provide a robust set of parsing primitives and enough
combinators to allow users to create any parsers they need.

Examples
--------

```python
import typing

import pcomb as pc

_T = typing.TypeVar("_T")
_BinaryOp = typing.Callable[[_T, _T], _T]


def add(x: float, y: float) -> float:
    return x + y


def sub(x: float, y: float) -> float:
    return x - y


def apply_op(values: typing.Tuple[float, _BinaryOp[float], float]) -> float:
    left, operation, right = values
    return operation(left, right)


plus_token = pc.token("+")
minus_token = pc.token("-")

plus = pc.pmap(plus_token, lambda t: add)
minus = pc.pmap(minus_token, lambda t: sub)
operator = plus | minus

padded_op = (pc.padding() >> operator) << pc.padding()
value_seq = pc.seq3(pc.float_(), padded_op, pc.float_())
expr = pc.pmap(value_seq, apply_op)

plus_result = expr.parse("1+2")
assert isinstance(plus_result, pc.ParseSuccess)
assert plus_result.value == 3

minus_result = expr.parse("3-2")
assert isinstance(minus_result, pc.ParseSuccess)
assert minus_result.value == 1

error = expr.parse("abc")
assert isinstance(error, pc.ParseFailure)
```
