import pcomb.primitives as pm
import pcomb.results as rs
import pytest


@pytest.fixture
def int_parser():
    return pm.IntParser()


@pytest.fixture
def float_parser():
    return pm.FloatParser()


def test_int_parser(int_parser):
    result = int_parser.parse("1234")

    assert isinstance(result, rs.ParseSuccess)
    assert result.value == 1234
    assert result.rest == ""


def test_int_parser_nodigit(int_parser):
    result = int_parser.parse("a1234")

    assert isinstance(result, rs.ParseFailure)


def test_float_parser(float_parser):
    result = float_parser.parse("1234")

    assert isinstance(result, rs.ParseSuccess)
    assert result.value == 1234.0
    assert result.rest == ""


def test_float_parser_decimal(float_parser):
    result = float_parser.parse("1234.56")

    assert isinstance(result, rs.ParseSuccess)
    assert result.value == 1234.56
    assert result.rest == ""


def test_float_parser_decimal_char(float_parser):
    result = float_parser.parse("1234.abc")

    assert isinstance(result, rs.ParseSuccess)
    assert result.value == 1234.0
    assert result.rest == "abc"


def test_float_parser_nodigit(float_parser):
    result = float_parser.parse("a1234")

    assert isinstance(result, rs.ParseFailure)
