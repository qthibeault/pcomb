import pcomb.results as rs
import pytest


@pytest.fixture
def success():
    return rs.ParseSuccess("hello", " world")


@pytest.fixture
def failure():
    return rs.ParseFailure("Parse failed")


@pytest.fixture
def end():
    return rs.ParseEnd()


def test_map_success(success):
    result = success.map(lambda v: v.upper())

    assert isinstance(result, rs.ParseSuccess)
    assert result.value == "HELLO"
    assert result.rest == " world"


def test_map_failure(failure):
    result = failure.map(lambda v: v.upper())

    assert isinstance(result, rs.ParseFailure)
    assert result.reason == "Parse failed"


def test_map_end(end):
    with pytest.raises(RuntimeError):
        end.map(lambda v: v.upper())


def test_bind_success(success):
    result = success.bind(lambda r: rs.ParseSuccess(r.rest, r.value))

    assert isinstance(result, rs.ParseSuccess)
    assert result.value == " world"
    assert result.rest == "hello"


def test_bind_failure(failure):
    result = failure.bind(lambda r: rs.ParseSuccess("foo", " bar"))

    assert isinstance(result, rs.ParseFailure)
    assert result.reason == "Parse failed"


def test_bind_end(end):
    with pytest.raises(RuntimeError):
        end.bind(lambda r: rs.ParseSuccess("foo", " bar"))
