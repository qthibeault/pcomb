# pylint: disable=missing-module-docstring,missing-function-docstring

import re

import pytest

import pcomb.parsers as ps
import pcomb.results as rs


def test_eol():
    parser = ps.EOLParser()

    assert isinstance(parser.parse(""), rs.ParseEnd)
    assert isinstance(parser.parse("abc"), rs.ParseFailure)


def test_string():
    parser = ps.StringParser("foo")
    result = parser.parse("foobar")

    assert isinstance(result, rs.ParseSuccess)
    assert result.value == "foo"
    assert result.rest == "bar"

    result = parser.parse("barfoo")

    assert isinstance(result, rs.ParseFailure)


def test_regex():
    parser = ps.RegexParser(r"\d\s\w")
    result = parser.parse("1 something")

    assert isinstance(result, rs.ParseSuccess)
    assert isinstance(result.value, re.Match)
    assert result.value.group() == "1 s"
    assert result.rest == "omething"

    result = parser.parse("foobar")

    assert isinstance(result, rs.ParseFailure)


def test_sequenced():
    parser = ps.SequencedParser(ps.StringParser("foo"), ps.StringParser("bar"))
    result = parser.parse("foobar")

    assert isinstance(result, rs.ParseSuccess)
    assert result.value == ("foo", "bar")
    assert result.rest == ""

    result = parser.parse("spambar")

    assert isinstance(result, rs.ParseFailure)

    result = parser.parse("foospam")

    assert isinstance(result, rs.ParseFailure)


def test_optional():
    parser = ps.OptionalParser(ps.StringParser("foo"))
    result = parser.parse("foobar")

    assert isinstance(result, rs.ParseSuccess)
    assert result.value == "foo"
    assert result.rest == "bar"

    result = parser.parse("spambar")

    assert isinstance(result, rs.ParseSuccess)
    assert result.value is None
    assert result.rest == "spambar"


def test_mapped():
    parser = ps.MappedParser(ps.StringParser("foo"), lambda s: s.upper())
    result = parser.parse("foo")

    assert isinstance(result, rs.ParseSuccess)
    assert result.value == "FOO"

    result = parser.parse("bar")

    assert isinstance(result, rs.ParseFailure)


def test_iterative():
    parser = ps.IterativeParser(ps.StringParser("foo"), ps.StringParser("bar"))
    result = parser.parse("foo")

    assert isinstance(result, rs.ParseSuccess)
    assert result.value == "foo"

    result = parser.parse("bar")

    assert isinstance(result, rs.ParseSuccess)
    assert result.value == "bar"

    result = parser.parse("baz")

    assert isinstance(result, rs.ParseFailure)

    with pytest.raises(RuntimeError):
        parser = ps.IterativeParser()
        parser.parse("foo")


def test_repeated():
    parser = ps.RepeatedParser(ps.StringParser("foo"))
    result = parser.parse("foofoobar")

    assert isinstance(result, rs.ParseSuccess)
    assert result.value == ["foo", "foo"]
    assert result.rest == "bar"

    result = parser.parse("bar")

    assert isinstance(result, rs.ParseFailure)


def test_chained():
    parser = ps.ChainedParser(ps.StringParser("foo"), lambda r: rs.ParseSuccess(r.rest, r.value))
    result = parser.parse("foobar")

    assert isinstance(result, rs.ParseSuccess)
    assert result.value == "bar"
    assert result.rest == "foo"

    result = parser.parse("bar")

    assert isinstance(result, rs.ParseFailure)


def test_lazy():
    parser = ps.LazyParser(lambda: ps.StringParser("foo"))
    result = parser.parse("foobar")

    assert isinstance(result, rs.ParseSuccess)
    assert result.value == "foo"
    assert result.rest == "bar"

    result = parser.parse("bar")

    assert isinstance(result, rs.ParseFailure)
