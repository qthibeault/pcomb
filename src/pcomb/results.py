"""Types that represent the potential outcomes of a parse.

Each type attempts to emulate the Functor type by implementing a map operation. Each type also
attempts to emulate the Monad type by implementing bind, although the signature is not exact.
By providing these interfaces, these types make it easier to chain computations by allowing
users to avoid checking the types of the values before applying an operation.
"""

from __future__ import annotations

import dataclasses
import typing

_RT = typing.TypeVar("_RT", covariant=True)
_RU = typing.TypeVar("_RU")


@dataclasses.dataclass(frozen=True)
class ParseSuccess(typing.Generic[_RT]):
    """Representation of a successful parse.

    Attributes:
        value: The value produced by the parser
        rest: The input not consumed by the parser
    """

    value: _RT
    rest: str

    def map(self, transform: typing.Callable[[_RT], _RU]) -> ParseSuccess[_RU]:
        """Transform the value stored in a ParseSuccess.

        This method applies the transformation to the value stored in the value attribute, but
        does not affect the rest attribute.

        Args:
            transform: Value transformation function

        Returns:
            A new ParseSuccess instance containing the transformed value and the same rest value
        """

        return ParseSuccess(transform(self.value), self.rest)

    def bind(self, transform: typing.Callable[[ParseSuccess[_RT]], _RU]) -> _RU:
        """Transform this ParseSuccess instance into another type.

        This method provides the entire instance as an argument to the transform function and
        returns the result. This method can be used to replace this instance with a value of
        any type, but it is recommended to only return instances of ParseSuccess, ParseFailure,
        or ParseEnd.

        Args:
            transform: Instance transformation function

        Returns:
            The value returned by the transform function
        """

        return transform(self)


@dataclasses.dataclass(frozen=True)
class ParseFailure:
    """Representation of a failed parse.

    Attributes:
        reason: Message indicating the cause of the failure
    """

    reason: str

    def map(self, *args: typing.Any) -> ParseFailure:  # pylint: disable=unused-argument
        """N-Argument function that returns the same instance.

        This method exists to implement the Functor interface, but it does nothing with the
        arguments and returns the same instance.

        Args:
            *args: Any values

        Returns:
            This instance
        """

        return self

    def bind(self, *args: typing.Any) -> ParseFailure:  # pylint: disable=unused-argument
        """N-Argument function that returns the same instance.

        This method exists to implement the Monad interface, but it does nothing the arguments
        and returns the same instance.

        Args:
            *args: Any values

        Returns:
            This instance
        """

        return self


@dataclasses.dataclass(frozen=True)
class ParseFailures:

    reasons: list[str]

    def map(self, *args: typing.Any) -> ParseFailures:  # pylint: disable=unused-argument
        """N-Argument function that returns the same instance.

        This method exists to implement the Functor interface, but it does nothing with the
        arguments and returns the same instance.

        Args:
            *args: Any values

        Returns:
            This instance
        """

        return self

    def bind(self, *args: typing.Any) -> ParseFailures:  # pylint: disable=unused-argument
        """N-Argument function that returns the same instance.

        This method exists to implement the Monad interface, but it does nothing the arguments
        and returns the same instance.

        Args:
            *args: Any values

        Returns:
            This instance
        """

        return self


@dataclasses.dataclass(frozen=True)
class ParseEnd(typing.Generic[_RT]):
    """Representation of the end of a parse.

    No further computation should be done once a parse is finished.
    """

    value: _RT

    def map(self, transform: typing.Callable[[_RT], _RU]) -> ParseEnd[_RU]:
        """N-argument function that always raises a RuntimeError.

        Args:
            *args: Any values
        """

        return ParseEnd(transform(self.value))

    def bind(self, *args: typing.Any) -> typing.NoReturn:  # pylint: disable=no-self-use
        """N-Argument function that always raises a RuntimeError.

        Args:
            *args: Any values
        """

        raise RuntimeError()
