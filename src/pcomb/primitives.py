# pylint: disable=too-few-public-methods

"""Primitive datatype parsers."""

from __future__ import annotations

import pcomb.parsers as ps


class IntParser(ps.Parser[int]):
    """Parser for integer values.

    This parser will will consume at least one digit and parse the result into a int.

    Attributes:
        parser: The parser used to extract digits from the input
    """

    def __init__(self):
        self.parser = ps.RegexParser(r"-?\d+")

    def parse(self, text: str) -> ps.ParserResult[int]:
        """Given a string, parse an int.

        Args:
            text: input for the digit sub-parser

        Returns:
            ParseSuccess with an integer if the sub-parser is successful, or a ParseFailure
        """

        return self.parser.parse(text).map(lambda m: int(m.group()))


def int_() -> ps.Parser[int]:
    """Integer parser factory function."""

    return IntParser()


class FloatParser(ps.Parser[float]):
    """Parser for float values.

    This parser will consume at least one digit and parse the result into a float.

    Attributes:
        parser: The parser used to extract digits from the input.
    """

    def __init__(self):
        self.parser = ps.RegexParser(r"-?\d+\.?\d*")

    def parse(self, text: str) -> ps.ParserResult[float]:
        """Given a string, parse a float.

        Args:
            text: input for the digit sub-parser

        Returns:
            ParseSuccess with float if the sub-parser is successful, or a ParseFailure
        """

        return self.parser.parse(text).map(lambda m: float(m.group()))


def float_() -> ps.Parser[float]:
    """Float parser factory function."""

    return FloatParser()


def whitespace() -> ps.Parser[None]:
    """Parser factory for whitespace, which consists of one or more spacing characters."""

    return ps.MappedParser(ps.RegexParser(r"\s+"), lambda m: None)


def padding() -> ps.Parser[None]:
    """Parser factory for padding, which consists of zero or more spacing characters."""

    return ps.MappedParser(ps.RegexParser(r"\s*"), lambda m: None)


__all__ = ["int_", "float_", "whitespace", "padding"]
