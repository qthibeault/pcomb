# pylint: disable=too-few-public-methods

"""Parser implementations.

This module defines the implementations for some basic parsers. The parsers are implemented as
classes because they are easier to define and allow for operators to be overloaded, which are
used to make shorthand parser combinations. Some of the parsers in this module are take patterns
to parse, while other parsers in this module are combinators or transformers.

The functions in the combinators module should be preferred over instantiating these classes
directly.
"""

from __future__ import annotations

import abc
import logging
import math
import re
import typing

import pcomb.results as rs

_PT = typing.TypeVar("_PT", covariant=True)
_PU = typing.TypeVar("_PU")

ParserResult = typing.Union[
    rs.ParseSuccess[_PT], rs.ParseFailure, rs.ParseFailures, rs.ParseEnd[_PT]
]


class Parser(abc.ABC, typing.Generic[_PT]):
    """Abstract representation of a parser.

    A parser has a single type variable which indicates the value of a successful parse.
    A parser requires subclasses to implement the parse method, which accepts a string and returns
    a ParserResult. A ParserResult is a sum type that can be a ParseSuccess, ParseFailure, or
    ParseEnd, which indicates the result of the parse.
    """

    @abc.abstractmethod
    def parse(self, text: str) -> ParserResult[_PT]:
        """Given a string, parse into a ParseResult.

        Args:
            text: The input string to the parser

        Returns:
            A ParserResult indicating the outcome of the parse.
        """

        raise NotImplementedError()

    def __or__(self, other: Parser[_PT]) -> Parser[_PT]:
        if not isinstance(other, Parser):
            return NotImplemented

        return IterativeParser(self, other)

    def __lshift__(self, other: Parser[typing.Any]) -> Parser[_PT]:
        if not isinstance(other, Parser):
            return NotImplemented

        return MappedParser(SequencedParser(self, other), lambda r: r[0])

    def __rshift__(self, other: Parser[_PU]) -> Parser[_PU]:
        if not isinstance(other, Parser):
            return NotImplemented

        return MappedParser(SequencedParser(self, other), lambda r: r[1])

    def __and__(self, other: Parser[_PU]) -> Parser[typing.Tuple[_PT, _PU]]:
        if not isinstance(other, Parser):
            return NotImplemented

        return SequencedParser(self, other)


class EOLParser(Parser[None]):
    """Parser which represents the end of the input."""

    def parse(self, text: str) -> ParserResult[None]:
        """Given a string, determine if the end of input has been reached.

        Args:
            text: input string to the parser

        Returns:
            A ParseFailure if the input string is not empty, and a ParseEnd if it is.
        """
        logger = logging.getLogger("pcomb.EOLParser")
        logger.debug("Input: %s", text)

        if len(text) > 0:
            return rs.ParseFailure("expected end of input")

        return rs.ParseEnd(None)


class StringParser(Parser[str]):
    """Parser that represents a substring the input must begin with.

    Attributes:
        string: The substring the input must begin with
    """

    def __init__(self, string: str):
        self.string = string

    def parse(self, text: str) -> ParserResult[str]:
        """Given a string, determine if it begins with the stored substring.

        Args:
            text: input string to the parser

        Returns:
            A ParseSuccess if the string begins with the substring, and a ParseFailure otherwise.
        """
        logger = logging.getLogger("pcomb.StringParser")
        logger.debug("Input: %s", text)
        logger.debug("String: %s", self.string)

        if not text.startswith(self.string):
            return rs.ParseFailure(f"could not match string {self.string}")

        start = len(self.string)
        rest = text[start:]

        return rs.ParseSuccess(self.string, rest)


class RegexParser(Parser[re.Match]):
    """Parser that represents a regex that will be matched from the beginning of the input.

    Attributes:
        matcher: The regex compiled from the provided expression
    """

    def __init__(self, expr: str):
        self.matcher = re.compile(expr)

    def parse(self, text: str) -> ParserResult[re.Match]:
        """Given a string, determine if the stored regex matches from the beginning.

        This method does not return the matched string, but the match object created by the regex
        library. This allows users to create arbitrary regex groups or other constructs for later
        processing.

        Args:
            text: input string to the parser

        Returns:
            A ParseSuccess if a regex match is found, and a ParseFailure otherwise.
        """
        logger = logging.getLogger("pcomb.RegexParser")
        logger.debug("Input: %s", text)
        logger.debug("Matcher: %s", self.matcher)

        match = self.matcher.match(text)

        if not match:
            return rs.ParseFailure(f"could not match expression {self.matcher}")

        return rs.ParseSuccess(match, text[match.end() :])


class SequencedParser(Parser[typing.Tuple[_PT, _PU]]):
    """Run provided parsers in order and return a tuple of the results.

    This class represents the sequence of two sub-parser, which are run the order first->second.

    Attributes:
        first: The parser that comes first in the sequence
        second: The parser that comes second in the sequence
    """

    def __init__(self, first: Parser[_PT], second: Parser[_PU]):
        self.first = first
        self.second = second

    def parse(self, text: str) -> ParserResult[typing.Tuple[_PT, _PU]]:
        """Given a string, run both parsers in order and return a tuple of the results.

        Both parsers must succeed for the result to be returned, otherwise the ParseFailure object
        from the failing sub-parser will be returned instead. The input not consumed by the first
        parser will be used as input to the second parser.

        Args:
            text: input to the first parser

        Returns:
            A ParseSuccess with both sub-parser results if both succeed, otherwise a ParseFailure.
        """
        logger = logging.getLogger("pcomb.SequencedParser")
        logger.debug("Input: %s", text)

        first_result = self.first.parse(text)
        return first_result.bind(lambda r: self.second.parse(r.rest).map(lambda u: (r.value, u)))


class OptionalParser(Parser[typing.Optional[_PT]]):
    """Parser which represents a parse which is not required.

    Attributes:
        parser: The sub-parser which will be allowed to fail
    """

    def __init__(self, parser: Parser[_PT]):
        self.parser = parser

    def parse(self, text: str) -> ParserResult[typing.Optional[_PT]]:
        """Given a string, run the sub-parser and ignore any errors.

        If the stored sub-parser fails, a ParseSuccess of None is returned instead of a
        ParseFailure.

        Args:
            text: input string to the sub-parser

        Returns:
            A ParseSuccess object with a None value if the sub-parser fails, or the parsed result.
        """
        logger = logging.getLogger("pcomb.OptionalParser")
        logger.debug("Input: %s", text)

        result = self.parser.parse(text)

        if isinstance(result, rs.ParseFailure):
            return rs.ParseSuccess(None, text)

        return result


class MappedParser(Parser[_PT]):
    """Parser that represents a transformation of the output of another parser.

    Attributes:
        parser: Parser that produces the output to be transformed
        transform: Transformation function
    """

    def __init__(self, parser: Parser[_PU], transform: typing.Callable[[_PU], _PT]):
        self.parser = parser
        self.func = transform

    def parse(self, text: str) -> ParserResult[_PT]:
        """Given a string, run the sub-parser and then apply the tranform function to the result.

        If the sub-parser fails, the transformation is not applied.

        Args:
            text: input string to the sub-parser

        Returns:
            A ParseSuccess with the value returned by the transformation function if the
            sub-parser succeeds, otherwise a ParseFailure.

        """
        logger = logging.getLogger("pcomb.MappedParser")
        logger.debug("Input: %s", text)

        return self.parser.parse(text).map(self.func)


class IterativeParser(Parser[_PT]):
    """Parser that represents a set of potential parsers for an input.

    This class represents a set of alternatives for parsing an input. Each alternative can be
    tried before selecting a result to return.

    Attributes:
        parser: Iterable of sub-parsers
    """

    def __init__(self, *parsers: Parser[_PT]):
        if len(parsers) == 0:
            raise RuntimeError("Cannot create IterativeParser without providing alternatives")

        self.parsers = parsers

    def parse(self, text: str) -> ParserResult[_PT]:
        """Given a string, run each parser and select the success that consumes the most input.

        This method will try each sub-parser and record the result, before filtering out the
        successes and selecting the success that consumes the most input.

        Args:
            text: input to each sub-parser

        Returns:
            The ParseSuccess that consumes the most input if any of the sub-parsers succeed,
            otherwise a ParseFailure.
        """
        logger = logging.getLogger("pcomb.IterativeParser")
        logger.debug("Input %s", text)

        results = [parser.parse(text) for parser in self.parsers]
        successes = [result for result in results if isinstance(result, rs.ParseSuccess)]
        ends = [result for result in results if isinstance(result, rs.ParseEnd)]

        if len(successes) == 0 and len(ends) == 0:
            failures = typing.cast(typing.List[rs.ParseFailure], results)
            reasons = [failure.reason for failure in failures]

            return rs.ParseFailures(reasons)

        if len(ends) == 0:
            return min(successes, key=lambda s: len(s.rest))

        return ends[0]


def _repeat_parse(parser: Parser[_PT], text: str) -> typing.Iterable[rs.ParseSuccess[_PT]]:
    result = parser.parse(text)
    while isinstance(result, rs.ParseSuccess):
        yield result
        result = parser.parse(result.rest)


class RepeatedParser(Parser[typing.List[_PT]]):
    """Parser that represents the repeated application of a sub-parser.

    Attributes:
        parser: The sub-parser to apply repeatedly
        nmin: The minimum number of successful parser applications
        nmax: The maximum number of successful parser applications
    """

    def __init__(self, parser: Parser[_PT], nmin: float = 1, nmax: float = math.inf):
        self.parser = parser
        self.min = nmin
        self.max = nmax

    def parse(self, text: str) -> ParserResult[typing.List[_PT]]:
        """Given a string, repeatedly apply a parser until failure.

        Each iteration, the input not consumed by the parser is provided as input to parser again.

        Args:
            text: input to the sub-parser

        Returns:
            A ParseSucess of a list of values representing each successful application of the
            parser if the number of applications is between nmin and nmax.
        """
        logger = logging.getLogger("pcomb.RepeatedParser")
        logger.debug("Input: %s", text)
        logger.debug("Min count: %d", self.min)
        logger.debug("Max count: %d", self.max)

        results = list(_repeat_parse(self.parser, text))

        if len(results) < self.min:
            return rs.ParseFailure(f"Expected at least {self.min} values, got {len(results)}")

        if len(results) > self.max:
            return rs.ParseFailure(f"Expected at most {self.max} values, got {len(results)}")

        values = [result.value for result in results]
        rest = results[-1].rest if len(values) >= 1 else text

        return rs.ParseSuccess(values, rest)


_ChainedTransform = typing.Callable[[rs.ParseSuccess[_PU]], ParserResult[_PT]]


class ChainedParser(Parser[_PT]):
    """Parser that represents a transformation of a ParserResult.

    Attributes:
        parser: The sub-parser to transform the output of
        transform: Parser output transformation function
    """

    def __init__(self, parser: Parser[_PU], transform: _ChainedTransform[_PU, _PT]):
        self.parser = parser
        self.func = transform

    def parse(self, text: str) -> ParserResult[_PT]:
        """Given a string, apply the parser and transform the ParserResult if it is a ParseSuccess.

        The transformation function is only applied if the result of the sub-parser is a
        ParseSuccess, so this method cannot be used to transform a ParseFailure into a
        ParseSuccess but it can be used to transform a ParseSuccess into a ParseFailure.

        Args:
            text: input to the sub-parser

        Returns:
            The transformation the result of the sub-parser if it is a ParseSuccess, otherwise the
            untransformed result of the sub-parser.
        """
        logger = logging.getLogger("pcomb.ChainedParser")
        logger.debug("Input: %s", text)

        return self.parser.parse(text).bind(self.func)


class LazyParser(Parser[_PT]):
    """Parser that is not instantiated until a parse is requested.

    This parser is useful for representing recursive parsers where a portion of the parser is not
    defined yet, or is defined later in the module.

    Attributes:
        factory: A zero-argument function to construct parsers
    """

    def __init__(self, factory: typing.Callable[[], Parser[_PT]]):
        self.factory = factory
        self.parser: typing.Optional[Parser[_PT]] = None

    def parse(self, text: str) -> ParserResult[_PT]:
        """Given a string, construct a parser and apply it to the input.

        Args:
            text: input to the constructed parser

        Returns:
            The output of the constructed parser
        """
        logger = logging.getLogger("pcomb.LazyParser")
        logger.debug("Input: %s", text)

        if self.parser is None:
            self.parser = self.factory()

        return self.parser.parse(text)
