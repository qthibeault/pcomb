r"""Collection of functions to create parsers.

This module contains factory functions and combinators for the parsers defined in the parsers
and primitives module. These methods are preferred to using the class constructors because they
are shorter and easier to read.

    Typical usage example:

    # Primitives
    whitespace = regex(r"\s*")
    plus = token("+")
    number = float_()

    # Sequences
    value = lseq(number, whitespace)
    operator = lseq(plus, whitespace)

    # Expressions
    expr = seq2(lseq(value, operator), value)
    result = expr.parse("1+2").map(lambda x, y: x + y)
"""

from __future__ import annotations

import re
import typing

import pcomb.parsers as ps

_T = typing.TypeVar("_T")
_U = typing.TypeVar("_U")
_V = typing.TypeVar("_V")

_Seq2 = typing.Tuple[_T, _U]
_Seq3 = typing.Tuple[_T, _U, _V]
_BindFn = typing.Callable[[ps.ParserResult[_T]], ps.ParserResult[_U]]


def token(string: str) -> ps.Parser[str]:
    """Create a parser which matches a string exactly at the beginning of the input.

    The parser returned by this method will return a ParseSuccess object containing the matched
    string if the parse succeeds. If the parse fails, the parser will return a ParseFailure object.

    Args:
        string: The string to match

    Returns:
        A parser which will match the start of the input against the string
    """

    return ps.StringParser(string)


def regex(expr: str) -> ps.Parser[re.Match]:
    """Create a parser which matches on a regular expression.

    The parser returned by this method will return a ParseSuccess object containing the Match
    object created by the regular expression if the parse succeeds. If the parse fails, the
    parser will return a ParseFailure.

    Args:
        expr: The regular expression to be used for matching

    Returns:
        A parser which will use the expression to match the beginning of the input
    """

    return ps.RegexParser(expr)


def optional(parser: ps.Parser[_T]) -> ps.Parser[typing.Optional[_T]]:
    """Create a parser which does not fail.

    The parser returned by this method will return a ParseSuccess object containing the result
    of the provided parser if the parse succeeds, and a ParseSuccess object containing a None
    value if the parse fails.

    Args:
        parser: The parser which will be allowed to fail

    Returns:
        A parser which will return a ParseSuccess of None when the parser fails
    """

    return ps.OptionalParser(parser)


def pmap(parser: ps.Parser[_T], transform: typing.Callable[[_T], _U]) -> ps.Parser[_U]:
    """Map provided transformation over the result of a parser.

    Given a parser and a transformation of the parser's output, create a new parser which produces
    the result of the transformation.

    Args:
        parser: The parser that produces the output to transform
        transform: The function to transform the output value of the parser

    Returns:
        A parser which produces a value of the type of the output of the transform function
    """

    return ps.MappedParser(parser, transform)


def seq2(first: ps.Parser[_T], second: ps.Parser[_U]) -> ps.Parser[_Seq2[_T, _U]]:
    """Sequence two parsers.

    The parser returned by this method will run the two provided parsers in order and if both
    parsers succeed a ParseSuccess object will be returned with a tuple of the two results in
    the order they were provided. If either parser fails, the parser will return the ParseFailure
    object returned from the sub-parser.

    Args:
        first: The parser to run first
        second: The parser to run second

    Returns:
        A parser which runs the provided parsers in order and returns a tuple of the result values
    """

    return ps.SequencedParser(first, second)


def seq3(
    first: ps.Parser[_T], second: ps.Parser[_U], third: ps.Parser[_V]
) -> ps.Parser[_Seq3[_T, _U, _V]]:
    """Sequence three parsers.

    Like seq2, this method returns a parser that runs the 3 provided parsers in order and will
    return a ParseSuccess object with a tuple of the three results in the same order if all the
    parsers succeed, and a ParseFailure object if any of the parsers fail.

    Args:
        first: The parser to run first
        second: The parser to run second
        third: The parser to run third

    Returns:
        A parser which runs the provided parsers in order and returns a tuple of the return values
    """

    return pmap(seq2(seq2(first, second), third), lambda v: (v[0][0], v[0][1], v[1]))


def lseq2(first: ps.Parser[_T], second: ps.Parser[_U]) -> ps.Parser[_T]:
    """Sequence two parsers, dropping the right value.

    Like seq2, returns a parser which runs the provided parsers in order. The parser returns a
    ParseSuccess of the left value if both sub-parsers are successful, and a ParseFailure
    otherwise.

    Args:
        first: Parser to run first
        second: Parser to run second

    Returns:
        A parser which runs the provided parsers in order and only returns the first return value

    """

    return first << second


def rseq2(first: ps.Parser[_T], second: ps.Parser[_U]) -> ps.Parser[_U]:
    """Sequence two parsers, dropping the left value.

    Like seq2, but returns a parser which runs the provided parsers in order. The parser returns a
    ParseSuccess of the right value if both sub-parsers are successful, and a ParseFailure
    otherwise.

    Args:
        first: The parser to run first
        second: The parser to run second

    Returns:
        A parser which runs the provided parsers in order and only returns the second return value
    """
    return first >> second


def ptry(*parsers: ps.Parser[_T]) -> ps.Parser[_T]:
    """Try multiple parsers against the same input.

    The parser returned by this method will try all of the provided parsers against the same input
    and will return the result which consumes the most of the input.

    Args:
        *parsers: List of parsers to try

    Returns:
        A parser which tries all input parsers and selects the result which consumes the most input
    """

    return ps.IterativeParser(*parsers)


def pbind(parser: ps.Parser[_T], func: _BindFn[_T, _U]) -> ps.Parser[_U]:
    """Replace output of parser with the return of the transform.

    The parser returned by this method will provide the result of the parser to the transformation
    and return the value of the transformation function as the parser result. The transform
    function should return a type of ParserResult or undefined behavior will result.

    Args:
        parser: The parser to bind to
        func: The transform function of the parser result object

    Returns:
        A parser which returns the output of the transform function applied to the parser output
    """

    return ps.ChainedParser(parser, func)


def some(parser: ps.Parser[_T]) -> ps.Parser[typing.List[_T]]:
    """Try parser multiple times.

    The parser returned by this method will re-apply the parser multiple times against the result
    of the previous parse until no more parses can be done. The parser will return a ParseSuccess
    object if the sub-parser is successful one or more times.

    Args:
        parser: The parser to try multiple times

    Returns:
        A parser which will apply the parser until failure
    """

    return ps.RepeatedParser(parser)


def defer(factory: typing.Callable[[], ps.Parser[_T]]) -> ps.Parser[_T]:
    """Create a parser which is not constructed until a parse.

    A lazy parser takes a factory which produces parsers, but does not call the parser factory
    until a parse is requested. This allows for recursive parsers to be defined because they are
    not evaluated until later.

    Args:
        factory: Zero-argument function that produces parser instances

    Returns:
        A parser which will not create a parser until a parse is requested.
    """

    return ps.LazyParser(factory)


def eol() -> ps.Parser[None]:
    return ps.EOLParser()


__all__ = [
    "token",
    "regex",
    "optional",
    "pmap",
    "seq2",
    "seq3",
    "lseq2",
    "rseq2",
    "ptry",
    "pbind",
    "some",
    "defer",
    "eol",
]
